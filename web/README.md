# Le web

> Le **Web** est le terme communément employé pour parle du World Wide Web, ou WWW, traduit en français par la toile d’araignée mondiale. Il fait référence au système hypertexte fonctionnant sur le réseau informatique mondial internet.

##  

## Historique



Le "World Wide Web", plus communément appelé "Web" a été développé au CERN (Conseil Européen pour la Recherche Nucléaire) par le Britannique *Sir Timothy John Berners-Lee* et le Belge *Robert Cailliau* au début des années 90. 

À cette époque les principaux centres de recherche mondiaux étaient déjà connectés les uns aux autres, mais pour faciliter les échanges d'information Tim Berners-Lee met au point le système hypertexte. 

Le système hypertexte permet, à partir d'un document, de consulter d'autres documents en cliquant sur des mots clés. 

Ces mots "cliquables" sont appelés hyperliens et sont souvent soulignés et en bleu. Ces hyperliens sont plutôt connus aujourd'hui sous le simple terme de "liens".

![premier_site](assets/premier_site.png)

Cette première page web est toujours consultable ***[ici]( http://info.cern.ch/hypertext/WWW/TheProject.html).***

Tim Berners-Lee développe le premier navigateur web (logiciel permettant de lire des pages contenant des hypertextes), il l'appelle simplement "***WorldWideWeb***". Il faudra attendre 1993 et l'arrivée du navigateur web "NCSA Mosaic" pour que le web commence à devenir populaire en dehors du petit monde de la recherche.

Techniquement le web se base sur trois choses : le protocole *HTTP* (HyperText Transfert Protocol), les *URL* (Uniform Resource Locator) et le langage de description *HTML* (HyperText Markup Language). Nous aurons, très prochainement l'occasion de revenir sur ces trois éléments.

Une chose très importante à bien avoir à l'esprit : beaucoup de personnes confondent "web" et "internet". Même si le "web" "s'appuie" sur internet, les deux choses n'ont rien à voir puisqu'"internet" est un "réseau de réseaux" s'appuyant sur le protocole IP (voir le module Internet) alors que, comme nous venons de le voir, le web est la combinaison de trois technologies : HTTP, URL et HTML. D'ailleurs on trouve autre chose que le "web" sur internet, par exemple, les emails avec le protocole SMTP (Simple Mail Transfert Protocol) et les transferts de fichiers avec le protocole FTP (File Transfert Protocol).

-----------

## Comment ?

Les navigateurs interprètent des **codes** (HTML et CSS) afin d’afficher des pages Web. Il s’agit de langages informatiques qui permettent de créer des sites web.
Les langages **HTML** et **CSS** sont à la base du fonctionnement de tous les sites Web.

Nous allons nous intéresser à un acteur fondamental du développement web, le couple HTML+CSS (***Hyper Text Markup Langage et Cascading Style Sheets).***

Dans un premier temps, nous allons exclusivement nous intéresser au HTML. Qu'est-ce que le HTML, voici la définition que nous en donne Wikipedia :

*L’Hypertext Markup Language, généralement abrégé HTML, est le format de données conçu pour représenter les pages web. C’est un langage de balisage permettant d’écrire de l’hypertexte, d’où son nom. HTML permet également de structurer sémantiquement et de mettre en forme le contenu des pages, d’inclure des ressources multimédias, dont des images, des formulaires de saisie, et des programmes informatiques. Il permet de créer des documents interopérables avec des équipements très variés de manière conforme aux exigences de l’accessibilité du web. Il est souvent utilisé conjointement avec des langages de programmation (JavaScript) et des formats de présentation (feuilles de style en cascade).*

Pour l'instant, nous allons retenir deux éléments de cette définition «conçu pour représenter les pages web» et «un langage de balisage».

Grâce au HTML vous allez pouvoir, dans votre navigateur (Firefox, Chrome, Opera,....), afficher du texte, afficher des images, proposer des hyperliens (liens vers d'autres pages web), afficher des formulaires et même maintenant afficher des vidéos (grâce à la dernière version du HTML, l'HTML5).

HTML n'est pas un langage de programmation (comme le Python par exemple), ici, pas question de conditions, de boucles....c'est un langage de description.

Le langage CSS (Cascade Style Sheets, feuilles de style en cascade) est venu compléter le langage HTML en 1996. Son rôle est de gérer l’apparence de la page web (agencement, positionnement, décoration, couleurs, taille du texte, etc.).

Autrement dit, le contenu est écrit dans le fichier HTML et la mise en forme est écrite dans le fichier CSS.

Vous pouvez examiner le code html du site du lycée :

https://charlotte-perriand.enthdf.fr -> clic droit sur la page -> afficher code source 

Vous devriez avoir quelque chose comme ça :

![site_genech](assets/site_genech.png)

Nous verrons dans un prochaine exercice ce que tout cela signifie.

-------

## Ressources 

En attendant, si vous souhaitez en savoir plus avant d'attaquer les activités en classe, voici quelques ressources :

- [MDN Web Docs](https://developer.mozilla.org/fr/docs/Learn) : Une ressource complète, écrite par Mozilla, pour les développeurs web. Les guides d'apprentissage du HTML et du CSS sont excellents pour les débutants.
- [Codecademy](https://www.codecademy.com/learn/learn-html) : Des cours interactifs pour apprendre le HTML et le CSS. Leur format pratique est excellent pour se familiariser avec ces langages.
- [W3Schools](https://www.w3schools.com/html/) : Une autre excellente ressource avec des exemples de code et des quiz pour tester la compréhension. Il s'agit d'un site déployé par le World Wide Web Consortium, c'est à dire les personnes chargées de définir les règles d'utilisation du web.
- [freeCodeCamp](https://www.freecodecamp.org/learn/responsive-web-design/) : Un cours gratuit qui couvre à la fois le HTML et le CSS, ainsi que d'autres aspects de la conception web réactive.

- [FlexboxFroggy](https://flexboxfroggy.com/#fr): Probablement mon outil préféré pour apprendre le CSS. En utilisant les règles du CSS, vous verrez une grenouille se déplacer sur votre écran selon ce que vous aurez écrit.
-  [Grid Garden](https://cssgridgarden.com/#fr) qui est un jeu similaire pour apprendre le CSS Grid Layout. 
- [Code With Blocks](https://codewithblocks.com/new) : Permet de mieux se représenter l'arborescence d'une page html, à l'aide de blocs. 
- [CSS Diner](https://flukeout.github.io) : Permet d'apprendre à manier les selecteurs CSS.
- [SmolCSS](https://smolcss.dev) : Une fois que vous maitriserez le CSS...Dans une dizaine d'années donc.

Enfin, vous trouverez dans le répertoire [ressources](ressources/) plusieurs documents qui vous aideront.

-------------------

Auteur : Florian Mathieu

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.