# Représentation d'un texte en machine

### Le programme

![bo](assets/bo.png)

-----------



![Martine écrit en UTF-8](https://www.apprendre-en-ligne.net/bloginfo/images/humour/geek_martine-ecrit-en-utf-8.jpg)

Source : http://www.retourdemartine.free.fr/

Prenons l'alphabet courant `A`, `B`, `C`, ... `Z` et plaçons-le dans un tableau. Il est important de noter que chaque lettre de l'alphabet est associée à un indice spécifique dans le tableau.

#### Tableau de l'alphabet avec indices associés

<table> <tr> <th>0</th> <th>1</th> <th>2</th> <th>3</th> <th>4</th> <th>5</th> <th>6</th> <th>7</th> <th>8</th> <th>9</th> <th>10</th> <th>11</th> <th>12</th> <th>13</th> <th>14</th> <th>15</th> <th>16</th> <th>17</th> <th>18</th> <th>19</th> <th>20</th> <th>21</th> <th>22</th> <th>23</th> <th>24</th> <th>25</th> </tr> <tr> <td>A</td> <td>B</td> <td>C</td> <td>D</td> <td>E</td> <td>F</td> <td>G</td> <td>H</td> <td>I</td> <td>J</td> <td>K</td> <td>L</td> <td>M</td> <td>N</td> <td>O</td> <td>P</td> <td>Q</td> <td>R</td> <td>S</td> <td>T</td> <td>U</td> <td>V</td> <td>W</td> <td>X</td> <td>Y</td> <td>Z</td> </tr> </table>

Ces indices sont essentiels car ils forment la base de la représentation des caractères en machine. Chaque caractère est représenté par un nombre entier, ce qui signifie qu'il peut être représenté en binaire dans une machine.

**Définition**

L'entier associé à un caractère est appelé ***point de code*** de ce caractère (0 est le point de code de `A`. 25 le point de code de `Z`).

Nous appelons cette correspondance entre caractères et nombres l'***encodage des caractères***.

Il a existé et existe plusieurs encodages, nous allons discuter de quelques-uns d'entre eux.

-----

### Encodage ASCII

En 1960, l'ISO (International Standards Organization) a introduit l'encodage ASCII (American Standard Code for Information Interchange). Dans l'ASCII, chaque caractère est associé à un nombre binaire sur 7 bits.

![Table ASCII](https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/ASCII-Table.svg/738px-ASCII-Table.svg.png)

Source : Wikipédia

Par exemple, le "A" majuscule correspond au code binaire `1000001`, qui est équivalent à `65` en base 10 ou `41` en base 16.

**Question** : Combien de points de code (et donc de caractères) peuvent être représentés grâce à l'encodage ASCII ?

**Travail à effectuer**

1. Quel est le point de code et la représentation en binaire du caractère `a` ?
2. Comment, grâce à la représentation en binaire, peut-on savoir si une lettre est en majuscule ou en minuscule ?

L'encodage ASCII est pratique pour la langue anglaise, mais il est insuffisant pour d'autres langues qui utilisent des caractères accentués, comme le français. C'est pourquoi d'autres encodages ont été développés.

### Encodage ISO-8859-1

L'encodage ISO-8859-1, également appelé "latin1", étend l'ASCII en utilisant 8 bits pour chaque point de code, ce qui permet de représenter 256 caractères différents.

Ainsi, il est possible de représenter des caractères accentués français, mais pas des caractères d'autres langues comme le russe ou le chinois.

**Question** : Comment est codé le caractère `é` en ISO-8859-1 ?

### Encodage Unicode

Pour répondre aux besoins de représentation de tous les caractères de toutes les langues, l'Unicode a été créé. L'Unicode peut théoriquement représenter plus d'un million de caractères (1 114 112 pour être précis).

**Question** : Quel est le point de code du caractère `é` en Unicode ?

### Encodage UTF-8

Le point de code Unicode ne donne que la correspondance entre un caractère et un nombre, mais ne dit pas comment ce nombre est représenté en binaire. C'est le travail d'un encodage comme l'UTF-8.

L'UTF-8 est un encodage à longueur variable. Cela signifie que chaque point de code Unicode peut être représenté par une séquence d'octets de longueur différente.

Nombre d'octets en UTF-8 :

- 1 octet pour les points de code allant de 0 à 127 (ce qui inclut l'ASCII).
- 2 octets pour les points de code allant de 128 à 2047.
- 3 octets pour les points de code allant de 2048 à 65535.
- 4 octets pour les points de code allant de 65536 à 1 114 111.

Ainsi, tous les caractères ASCII sont codés en un seul octet, ce qui permet de conserver la compatibilité avec l'ASCII. Les caractères couramment utilisés dans la plupart des langues européennes, dont le français, sont codés en deux octets. Les autres caractères, y compris les caractères de langues comme le chinois, le japonais, le coréen, etc., sont codés en trois ou quatre octets.

**Exemple pratique** : Supposons que nous voulions encoder le texte "Bonjour le monde!" en UTF-8. Pour chaque caractère, nous rechercherions son point de code Unicode, puis déterminerions combien d'octets sont nécessaires pour le représenter en UTF-8. Par exemple, le caractère 'B' a un point de code Unicode de 66, donc il serait encodé en un octet en UTF-8. Le caractère 'o' a un point de code Unicode de 111, donc il serait également encodé en un octet, et ainsi de suite pour tous les caractères du texte.

----------

**Ressources supplémentaires**

Pour approfondir le sujet, vous pouvez consulter les liens suivants :

1. [Unicode](https://home.unicode.org/)
2. [UTF-8](https://fr.wikipedia.org/wiki/UTF-8)

La représentation d'un texte en machine est un sujet complexe, mais essentiel pour la compréhension du fonctionnement des ordinateurs et du Web. En comprenant comment chaque caractère est représenté en binaire, vous pouvez mieux comprendre comment les ordinateurs traitent et stockent les informations.

--------

Auteur : Florian Mathieu

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.
