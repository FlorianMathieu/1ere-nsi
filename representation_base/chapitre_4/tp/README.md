## Mystère

### Description

L'idée de ce TP est de manipuler les expressions booléennes.

Pour cela, vous allez devoir programmer des fonctions booléennes.

A chaque fonction `mystere` suivante, sont données des doctests. Ces doctests correspondent aux valeurs des tables de vérités.

### Consignes

Recopier le bloc de code suivant dans un fichier `mystere.py`

Vous devez écrire le corps de la fonction sous la forme d'une expression booléenne, dont les variables correspondent aux paramètres d'entrée fournis.

```python
def mystere1(a):
    '''
    Cette fonction renvoie l'inverse du paramètre a.
    
    >>> mystere1(True)
    False
    >>> mystere1(False)
    True
    '''
    # Votre code ici.
    pass

def mystere2(a, b):
    '''
    Cette fonction renvoie True si au moins l'un des paramètres est True.
    
    >>> mystere2(True, True)
    True
    >>> mystere2(True, False)
    True
    >>> mystere2(False, True)
    True
    >>> mystere2(False, False)
    False
    '''
    # Votre code ici.
    pass

def mystere3(a, b):
    '''
    Cette fonction renvoie True si les paramètres sont différents l'un de l'autre.
    
    >>> mystere3(True, True)
    False
    >>> mystere3(True, False)
    True
    >>> mystere3(False, True)
    True
    >>> mystere3(False, False)
    False
    '''
    # Votre code ici.
    pass

def mystere4(a, b):
    '''
    Cette fonction renvoie True seulement si les deux paramètres sont False.
    
    >>> mystere4(True, True)
    False
    >>> mystere4(True, False)
    False
    >>> mystere4(False, True)
    False
    >>> mystere4(False, False)
    True
    '''
    # Votre code ici.
    pass

def mystere5(a, b):
    '''
    Cette fonction est une équivalence, elle renvoie True si les deux paramètres ont la même valeur.
    
    >>> mystere5(True, True)
    True
    >>> mystere5(True, False)
    False
    >>> mystere5(False, True)
    False
    >>> mystere5(False, False)
    True
    '''
    # Votre code ici.
    pass

def mystere6(a, b):
    '''
    Cette fonction renvoie True si le premier paramètre est True et le deuxième est False.
    
    >>> mystere6(True, True)
    False
    >>> mystere6(True, False)
    True
    >>> mystere6(False, True)
    False
    >>> mystere6(False, False)
    False
    '''
    # Votre code ici.
    pass

def mystere7(a, b):
    '''
    Cette fonction renvoie True si le premier paramètre est False et le deuxième est True, ou si les deux paramètres sont False.
    
    >>> mystere7(True, True)
    False
    >>> mystere7(True, False)
    False
    >>> mystere7(False, True)
    True
    >>> mystere7(False, False)
    True
    '''
    # Votre code ici.
    pass

if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
```

A la fin de ce TP, n'oubliez pas de vérifier que toutes les fonctions passent les doctests. Vous pouvez exécuter votre fichier Python en ligne de commande pour voir les résultats des tests. Si une fonction ne passe pas un doctest, revoyez votre logique booléenne pour cette fonction et essayez à nouveau.





Auteur : Florian Mathieu

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.