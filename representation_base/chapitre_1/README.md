# Représentation des entiers naturels

## Le programme


![bo_1.png](assets/bo.png)



> Pour que vous compreniez le fonctionnement du binaire, et des systèmes de comptage en général (plus communément appelés bases), je vais commencer par faire une petite réintroduction à la base 10 que vous connaissez tous et toutes.

> En effet, tout le monde sait compter en base 10 (décimal). Mais comment ça marche ? Comment est construit notre système ? Pour répondre à cette question à l'apparence simple, oubliez tout et reprenons depuis le début : comment avez-vous appris à compter à l'école ?

---------

## La numération décimale (base 10 )

Dans la vie courante et dans beaucoup de domaines, nous utilisons la numération décimale. Elle repose à l’origine sur nos dix doigts : les dix symboles – chiffres – permettent de représenter tous les nombres.

La position des chiffres est primordiale dans cette représentation (numération de position) : il y a quelques années déjà, vous avez appris ce qu’étaient les unités (colonne de droite), les dizaines, les centaines, etc…

Bref, il y a 10 chiffres : 0, 1, 2, 3, 4, 5, 6, 7, 8, 9. 

Avec ces derniers, on peut compter jusqu'à 9. Et si l'on veut aller au-delà de 9, il faut changer de rang. Le nombre en est ainsi décomposé. 

Ainsi, on peut écrire 4138 comme 4 * 1000 + 1 * 100 + 3 * 10 + 8 * 1

- On remarque les égalités suivantes : 1000 = 10<sup>3</sup> ; 100 = 10² ; 10 = 10<sup>1</sup> ; 1 = 10<sup>0</sup>

Donc 4138 peut s’écrire : 4 * 10<sup>3</sup> + 1 * 10<sup>2</sup> + 3 * 10<sup>1</sup> + 8 * 10<sup>0</sup> 

- où 10 est appelé BASE de cette numération (ici décimale)
- où chaque chiffre (compris entre 0 et 9) est soit celui des unités, dizaines, etc…

***Application : méthode des divisions succèssives par 10 (car on parle de base 10) :***

4138 divisé par 10 = ....          ---> reste ....
.... divisé par 10 = ....           ---> reste ....
.... divisé par 10 = ....        ---> reste ....
.... divisé par 10 = ....    ---> reste ....


Soit .... x 10<sup>3</sup> + .... x 10<sup>2</sup> + .... x 10 <sup>1</sup> + .... x 10<sup>0</sup> = ................

*Un nombre est égal à la somme des valeurs de ses rangs, et on peut décomposer n'importe quel nombre en
puissance de sa base.* 

-----------

## Le codage binaire (base 2)



Nous avons donc vu le principe de **rangs** dans l'écriture d'un nombre.

En binaire, c'est pareil à la différence qu'on utilise le terme ***bit***, qui est la
contraction de **_binary digit_**, littéralement **_chiffre binaire_**. 

Un bit a deux états stables.
En électronique, il est facile d'obtenir un système présentant deux états stables distincts.

Prenons l'exemple d'un interrupteur : 

![interrupteur.png](assets/interrupteur.png)

Exemple d'information à deux états : 

| Chiffre binaire | États           | Interrupteur | DEL     | Tension Présente | Logique |
| --------------- | --------------- | ------------ | ------- | ---------------- | ------- |
| 0               | Passif / Repos  | Ouvert       | Éteinte | Non              | FAUX    |
| 1               | Actif / Travail | Fermé        | Allumée | Oui              | VRAI    |

Ainsi, pour coder une information qui peut ne prendre que deux états stables, la numération binaire est la
plus adaptée.

**Remarque** : étant donné que les symboles 0 et 1 sont communs à beaucoup de bases de numération (en
l’occurrence 2 et 10), nous adoptons les notations suivantes.

- (1011)<sub>b</sub> ou 1011(en base 2) ou encore (1011)<sub>2</sub> pour la base binaire 

- (101)<sub>d</sub> ou 101(en base 10) ou encore (101)<sub>10</sub> pour la base dix

### Comment trouver la représentation en base deux d'un entier naturel donné en base dix 

-  Méthode des divisions successives : 
- Exemple: (11)<sub>d</sub> = (?)<sub>b</sub>

![10_vers_2.png](assets/10_vers_2.png)

(11)<sub>d</sub> => (1011)<sub>b</sub>

###  Comment représenter des informations complexes ? 

- Avec 1 bit, nous pouvons coder 2 informations.
- Avec 2 bits, nous pouvons coder 4 informations différentes (2²)

Si nous généralisons un peu : avec **_k_** bits, nous pouvons coder **_2<sup>k</sup>_** informations différentes

### À faire vous-même 

Compléter le tableau suivant afin de coder les 8 premiers entiers naturels (entiers positifs ou nul)

| Bit de poids fort |                 | Bit de poids faible | Base 10 |
| ----------------- | --------------- | ------------------- | ------- |
| Bit<sub>2</sub>   | Bit<sub>1</sub> | Bit<sub>0</sub>     |         |
|                   |                 |                     | 0       |
|                   |                 |                     | 1       |
|                   |                 |                     | 2       |
|                   |                 |                     | 3       |
|                   |                 |                     | 4       |
|                   |                 |                     | 5       |
|                   |                 |                     | 6       |
|                   |                 |                     | 7       |

***Remarques*** : 

- le bit de poids fort est le bit le plus à gauche
- le bit de poids faible est donc celui le plus à droite

### À faire vous-même 

1. Convertir 42(10) en base 2 
2. Convertir 104(10) en base 2

### Qu'est ce qu'un octet ?

Un octet ((**byte** en anglais) est un regroupement de 8 bits.
On parle aussi de mot. Il permet de coder 2<sup>8</sup> = 256 mots différents.
Si nous codons des entiers naturels, nous coderons les nombres 0 à 255. Dans la littérature, un regroupement de 4 bits est appelé un quartet (cela nous servira plus tard).

![octet.png](assets/octet.png)

--------

### Unités de mesure

Il est très courant en informatique de mesurer la capacité mémoire d'un disque dur, de la RAM d'un ordinateur ou d'un débit de données Internet avec une unité de mesure exprimée comme un multiple d'octets. Ces multiples sont traditionnellement des puissances de 10 et on utilise les préfixes "kilo", " méga", etc. pour les nommer. Le tableau ci-dessous donne les principaux multiples utilisés dans la vie courante.

| Nom              | Symbole            | Valeur |
| :--------------- |:---------------:   | -----:|
| Kilooctet  |   ko                     |  10<sup>3</sup> octets |
| Mégaoctet  |   Mo                     |  10<sup>3</sup> ko |
| Gigaoctet  |   Go                     |  10<sup>3</sup> Mo |
| Teraoctet  |   To                     |  10<sup>3</sup> Go |

> Remarque : Historiquement, les multiples utilisés en informatique étaient des puissances de 2. Pour ne pas confondre l'ancienne et la nouvelle notation, on utilise des symboles différents pour représenter ces multiples.

| Nom              | valeur            | Nombre d'octets |
| :--------------- |:---------------:   | -----:|
| Kio  |   2<sup>10</sup> octets                 |  1024 |
| Mio  |   2<sup>10</sup>Kio                     |1048576|
| Gio  |   2<sup>10</sup>Mio                     |1073741824|
| Tio  |   2<sup>10</sup>Gio                     |1099511627776|

### À faire vous-même 

> Faisons la conversion de la base 2 vers la base 10 --> Passer de (0 1 1 0 1 1 0 1)<sub>b</sub> = (….......)<sub>d</sub>

***Méthode :*** 

- Ecrire le nombre binaire dans le tableau de correspondance
- Faire la somme des valeurs des rangs pour lesquels la valeur du bit vaut 1.

| Rang                       | 7             | 6             | 5             | 4             | 3             | 2             | 1             | 0             |
| -------------------------- | ------------- | ------------- | ------------- | ------------- | ------------- | ------------- | ------------- | ------------- |
| Puissance                  | 2<sup>7</sup> | 2<sup>6</sup> | 2<sup>5</sup> | 2<sup>4</sup> | 2<sup>3</sup> | 2<sup>2</sup> | 2<sup>1</sup> | 2<sup>0</sup> |
| Pondération                | 128           | 64            | 32            | 16            | 8             | 4             | 2             | 1             |
| Nombre binaire à convertir |               |               |               |               |               | 1             | 1             | 1             |

Somme : ?

-------

### Le système hexadecimal

La représentation en binaire n'est pas pratique à nous humain pour travailler (longueur de l'information importante, difficile à écrire et à lire sans faire d'erreur...).

Pour cela, nous travaillons avec la ***base hexadécimale.*** 

Le système hexadécimal permet de réduire la longueur des mots et facilite leur manipulation :

L'écriture d'un nombre binaire en base hexadécimale est aisée.


Ce système comporte seize symboles :
- les dix chiffres du système décimal (0 à 9)

- et les six premières lettres de l’alphabet (A à F) 

  Ce sera donc un système en **base 16**.

  

- Pour l'ordinateur, ça ne change rien, il travaille toujours en binaire. 

### À faire vous-même

> Compléter la colonne binaire

| Décimal | Binaire | Hexadécimal |
| ------- | ------- | ----------- |
| 0       | 0000    | 0           |
| 1       | 0001    | 1           |
| 2       | 0010    | 2           |
| 3       | 0011    | 3           |
| 4       |         | 4           |
| 5       |         | 5           |
| 6       |         | 6           |
| 7       |         | 7           |
| 8       |         | 8           |
| 9       |         | 9           |
| 10      |         | A           |
| 11      |         | B           |
| 12      |         | C           |
| 13      |         | D           |
| 14      |         | E           |
| 15      |         | F           |

> Passer de la base décimale à la base hexadécimale 

Application : la méthode des divisions successives dans la base 16 :

63650 divisé par 16 = **3978**         ---> reste **2**
**3978** divisé par 16 = ....           ---> reste ....
.... divisé par 16 = ....        ---> reste ....
.... divisé par 16 = ....    ---> reste ....

On obtient alors la combinaison de nombres hexadécimaux suivants : ....................


Soit .... x 16<sup>3</sup> + .... x 16<sup>2</sup> + .... x 16<sup>1</sup> + .... x 16<sup>0</sup> = ................

> Faisons la conversion de la base 16 vers la base 10, écrire le nombre 2A3 (16) en base décimale

Méthode : 

- Ecrire le nombre hexadécimal dans le tableau de correspondance en positionnant le chiffre correspondant à chacun des rangs.
-  Faire la somme des produits des chiffres avec la pondération correspondante.

| Rang                           | 3              | 2              | 1              | 0              |
| ------------------------------ | -------------- | -------------- | -------------- | -------------- |
| Puissance                      | 16<sup>3</sup> | 16<sup>2</sup> | 16<sup>1</sup> | 16<sup>0</sup> |
| Pondération                    | 4096           | 256            | 16             | 1              |
| Nombre hexadécimal à convertir |                |                |                |                |



> Passer du code binaire au code hexadécimal

**_Première méthode_** : opérer en deux étapes.

- passer du binaire au décimal dans un premier temps
- passer ensuite du décimal à l’hexadécimal    
  Exemple : vérifier que 10110111101 (2) = 1469 (10) = 5BD


**_Deuxième méthode_** : plus rapide, elle consiste à découper le nombre binaire en quartets (mots de 4 bits), à partir de la droite, puis à remplacer chaque quartet par le symbole hexadécimal correspondant.

Exemple : 10110111101 (2) = 101 1011 1101 en binaire découpé en quartet 
                          =  5   B    D   en hexadécimal

--------

### Passer d'une base quelconque à une autre

Pour passer d'une base à une autre, on passera par la base 10 car c'est sur cette base qu'on maîtrise le mieux les opérations de base.

Exemple : (944)<sub>10</sub> → ( 12234)<sub>5</sub>

![base.png](assets/base.png)

--------

Auteur : Florian Mathieu

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.
