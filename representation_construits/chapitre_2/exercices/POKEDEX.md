### Mini- Projet



Pour remplir le pokedex de Sacha, on modélise les informations sur les Pokemons de la façon suivante :

```python
liste_pokemons = {
  'Bulbizarre' : (70, 7),
  'Herbizarre' : (100,13),
  'Abo' : (200, 7),
  'Pikachu' : (40, 6)
}
```

Les tuples représentent la taille en centimètres, ainsi que le poids en kilogramme du Pokemon.

#### Questions :

- Quel est le type de *liste_pokemons* ?

- Quelle instruction permet d'ajouter à cette structure de données le Pokemon Goupil qui mesure 60 cm et pèse 10 kg ?

On donne le code suivant :

```python
def le_plus_grand(pokemons):
  grand = 0
  taille_max = 0
  for (nom,(taille, poids)) in pokemons.items():
    if taille_max < taille:
      taille_max = taille
      grand = nom
  return(grand,taille_max)
```

- Quelle est la valeur de *le_plus_grand(liste_pokemons)* ?

- Écrire le code d'une fonction *le_plus_leger(liste_pokemons)* qui prend des Pokémons en paramètre et renvoie un tuple correspondant au nom ainsi qu'au poids de Pokémon le plus léger.

```python
>> le_plus_leger(liste_pokemons)
>> ('Pikachu', 6)
```



- Écrire une fonction *taille* qui prend en paramère un dictionnaire de Pokémons ainsi que le nom d'un Pokémon, et qui renvoie la taille de ce Pokémon.

```python
>> taille(liste_pokemons, 'Abo')
>> 200
```

```python
>> taille(liste_pokemons, 'Dracaufeu')
>> None
```



----------



À faire dans l'année :

1. **Recherche par critères** : Ajoutez une fonctionnalité qui permet à l'utilisateur de rechercher des Pokémons en fonction de critères spécifiques, tels que la taille, le poids, le type, etc. Cela pourrait être implémenté en demandant à l'utilisateur de fournir les critères de recherche et en filtrant les Pokémons en conséquence.
2. **Évolution des Pokémons** : Ajoutez des informations sur l'évolution des Pokémons. Par exemple, vous pouvez ajouter une clé supplémentaire dans le dictionnaire des Pokémons qui indique le Pokémon suivant dans la chaîne d'évolution. Cela permettrait de créer une relation entre les Pokémons et d'afficher les évolutions possibles.
3. **Statistiques des Pokémons** : Ajoutez des statistiques supplémentaires pour chaque Pokémon, telles que les points de vie, l'attaque, la défense, etc. Ces statistiques pourraient également être stockées dans des tuples ou dans des dictionnaires séparés, associés aux noms des Pokémons.
4. **Interface utilisateur graphique (GUI)** : Créez une interface utilisateur graphique pour rendre l'utilisation du Pokedex plus conviviale. Vous pouvez utiliser des bibliothèques comme Tkinter ou PyQt pour construire l'interface.
5. **Enregistrement des données** : Ajoutez la possibilité d'enregistrer les données des Pokémons dans un fichier ou une base de données, de sorte que les informations ne soient pas perdues lorsque le programme est fermé. Vous pouvez utiliser des formats de données tels que JSON ou CSV pour stocker les informations des Pokémons.

Nous verrons au cours de l'année les méthodes pour ajouter ces fonctionnalités.

-------

Source :

- Gitlab Université de Lille 
