### Exercice 1

#### Itérer sur les éléments d'un dictionnaire

Au zoo de Beauval, il y a 5 éléphants d'Asie, 17 écureuils d'Asie, 7 hippopotames d'Afrique...

On représente cet inventaire à l'aide d'un dictionnaire, de la façon suivante:

```python
zoo_Beauval = {
'elephant' : ('Asie', 5),
'ecureuil' : ('Asie', 17),
'panda' : ('Asie', 2),
'hippopotame' : ('Afrique', 7),
'girafe' : ('Afrique', 4)
}
```

De la même manière, on peut représenter le zoo de La Flèche :

```python
zoo_LaFleche = {
  'ours' : ('Europe', 4),
  'tigre' : ('Asie', 7),
  'girafe' : ('Afrique', 11),
  'hippopotame' : ('Afrique', 3)
  }
```



##### Question n°1

On souhaite créer une fonction *plus_grand_nombre* ( ) qui prend un zoo en paramètre et renvoie le nom de l'animal le plus présent au sein du zoo.

Exemples :

```python
>> plus_grand_nombre(zoo_LaFleche)
>> 'girafe'
```

```python
>> plus_grand_nombre(zoo_Beauval)
>> 'ecureuil'
```

On aura besoin d'une boucle utilisant 

```python
for (cle, valeur) in dico.items()
```

À votre avis, pourquoi ?

**Nous avons besoin à la fois des clés ainsi que des valeurs**

Écrire la fonction :

```python
def plus_grand_nombre(zoo):
  """
  :param: zoo est un dictionnaire dont les clés sont des str (noms des animaux) 
  :param: les valeurs de ces clés sont des tuples (origine, nombre) avec origine : str et nombre : int
  :return: le nom de l'animal le plus représenté dans le zoo, sous la forme d'une chaîne de caractères
  """
  
  nom_max = 0
  nombre_max = 0
  for (nom, (x, nombre)) in zoo.items():
    if nombre > nombre_max:
      nom_max = nom
      nombre_max = nombre
  return nom_max 
```



Note : ici, la variable **x** fait référence à une donnée que nous n'utilisons pas.

##### Question n°2

On souhaite se doter d'une fonction *nombre_total* ( ) qui prend un zoo ainsi que le nom d'un continent en paramètre, et qui renvoie le nombre d'animaux originaires de ce continent dans le zoo.

Exemples :

```python
>> nombre_total(zoo_LaFleche, 'Afrique')
>> 14
```

```python
>> nombre_total(zoo_Beauval, 'Asie')
>> 24
```

On utilisera une boucle 

```python
for valeur in dico.values()
```

Pourquoi ?

**Parce que l'on a uniquement besoin des valeurs, l'espèce animal n'est pas importante**

Écrire la fonction :

```python
def nombre_total(zoo, continent):
  """
  :param: zoo est un dictionnaire dont les clés sont des chaines, correspondantes aux noms des animaux
  :param: et dont les valeurs sont des tuples (origine, nombre), origine étant une chaine, nombre un int
  :param: continent est une chaine comprenant le nom d'un continent d'où sont originaires les animaux
  :return: la fonction renvoie le nombre d'animaux originaires de 'continent' dans ce zoo
  """
  
  
  total = 0
  for (origine, nombre) in zoo.values():
    if continent == origine:
      total = total + nombre
  return total
```



##### Question n°3

Enfin, on souhaite écrire une fonction nombre, qui prendun zoo ainsi qu'un nom d'animal en paramètre, et qui renvoie le nombre de représentants de cet animal dans le zoo.

Exemples :

````python
>> nombre(zoo_LaFleche, 'panda')
>> 0
````

```python
>> nombre(zoo_Beauval, 'panda')
>> 2
```



Quel type de boucle va t-on utiliser ici ?

**Aucune, car nous n'avons uniquement besoin que de la clé, qui est un paramètre de la fonction.**

Écrire la fonction

```python
def nombre(zoo, animal):
  """
  :param: zoo est un dictionnaire dont les clés sont des chaines, correspondantes aux noms des animaux
  :param: et dont les valeurs sont des tuples (origine, nombre), origine étant une chaine, nombre un int
  :param: animal est une chaine comprenant le nom d'un animal
  :return: la fonction renvoie le nombre de représentants du paramètre 'animal' dans ce zoo
  """
  
  if animal not in zoo.keys():
    return 0
  else:
    return zoo[animal][1]
```

--------

### Exercice 2



##### Question 1



```python
# Création du dictionnaire
livre = {
    "titre": "1984",
    "auteur": "George Orwell",
    "annee_publication": 1949
}

# Modification des informations
livre["annee_publication"] = 1950

# On affiche des détails du livre
for cle, valeur in livre.items():
    print(f"{cle}: {valeur}")
```



##### Question 2

```python
# Création du dictionnaire
liste_courses = {
    "pommes": 5,
    "bananes": 10,
    "lait": 2
}

# Parcours du dictionnaire et affichage des articles
for article, quantite in liste_courses.items():
    print(f"{article}: {quantite}")

```



##### Question 3

```python
# Définir une fonction pour vérifier si une clé est dans un dictionnaire
def est_present(dictionnaire, cle):
    if cle in dictionnaire:
        return True
    else:
        return False

# Test
dictionnaire_test = {"pomme": 1, "banane": 2, "cerise": 3}
(est_present(dictionnaire_test, "pomme"))
True
(est_present(dictionnaire_test, "orange"))
False
```



##### Question 4

```python
# Créer un carnet d'adresses vide
carnet_adresses = {}

# Définir une fonction pour ajouter un nouveau contact
def ajouter_contact(nom, telephone):
    carnet_adresses[nom] = telephone

# Définir une fonction pour modifier un contact existant
def modifier_contact(nom, telephone):
    if nom in carnet_adresses:
        carnet_adresses[nom] = telephone

# Définir une fonction pour supprimer un contact
def supprimer_contact(nom):
    if nom in carnet_adresses:
        del carnet_adresses[nom]

# Tester les fonctions
ajouter_contact("Alice", "06-12-34-56-78")
ajouter_contact("Bob", "06-87-65-43-21")
modifier_contact("Alice", "07-12-34-56-78")
supprimer_contact("Bob")

# Afficher le carnet d'adresses pour vérifier qu'il fonctionne correctement
print(carnet_adresses)
```



##### Question 5

```python
# Définir une fonction pour vérifier si une clé est dans un dictionnaire
def est_present(dictionnaire, cle):
    if cle in dictionnaire:
        return True
    else:
        return False

# Test
dictionnaire_test = {"pomme": 1, "banane": 2, "cerise": 3}
(est_present(dictionnaire_test, "pomme"))
True
(est_present(dictionnaire_test, "orange"))
False
```

