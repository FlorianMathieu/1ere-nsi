## TP : Jeu de Mastermind

## Introduction

Le Mastermind est un jeu de réflexion qui se joue à deux personnes. L'objectif du jeu est de faire deviner à une première personne une combinaison secrète de couleurs choisie par l'autre élève en un nombre limité de tentatives. Dans ce TP, nous allons créer un jeu de Mastermind en utilisant des listes en Python.

-----------------------------



## Consignes

Il faudra créer une fonction python Mastermind.py qui va permettre de respecter les règles suivantes :

1. **Initialisation du jeu**

   - Générez une combinaison secrète de 4 couleurs choisies parmi les couleurs suivantes : Rouge (R), Bleu (B), Vert (V), Jaune (J), Orange (O) et Rose (P). Utilisez une liste pour stocker cette combinaison secrète.

   

2. **Interaction entre élèves**

   - Demandez à votre camarade de TP de deviner la combinaison secrète en entrant une liste de 4 lettres représentant les couleurs (par exemple, 'R', 'B', 'V', 'J'). Assurez-vous que les lettres sont toutes en majuscules.
   - Le joueur dispose de 10 tentatives pour trouver la combinaison secrète.

3. **Vérification de la proposition**

   - Comparez la proposition du joueur avec la combinaison secrète et fournissez des indices pour indiquer si les couleurs sont correctes et bien placées (`X`) ou si les couleurs sont correctes mais mal placées (`O`). 
   - Utilisez une deuxième liste pour stocker ces indices.
   - Par exemple, si la combinaison secrète est ['R', 'B', 'V', 'J'] et la proposition du joueur est ['B', 'V', 'R', 'J'], les indices seront ['O', 'O', 'X', 'X'].

4. **Affichage des indices**

   - Affichez les indices pour aider le joueur à ajuster sa prochaine proposition. Par exemple, "O O X X" pour les indices de l'exemple précédent.

5. **Répétez les étapes 2 à 4**

   - Demandez au joueur de faire une autre proposition en répétant les étapes de vérification et d'affichage des indices jusqu'à ce que le joueur trouve la combinaison secrète ou épuise toutes les tentatives.

6. **Fin du jeu**

   - Si le joueur a trouvé la combinaison secrète, affichez un message de félicitations. Sinon, affichez un message indiquant que le joueur a échoué à trouver la combinaison secrète.

   

   ```python
   def mastermind(combinaison, proposition):
       """
       Compare la proposition du joueur avec la combinaison secrète et renvoie les indices correspondants.
       :param combinaison : liste contenant la combinaison secrète
       :param proposition : liste contenant la proposition de l'adversaire
       :return indices : liste contenant les indices pour la proposition du joueur
       
       """
   pass
   ```
   
   
   

Utilisation : 

```python
   combinaison_secrete = ['R', 'B', 'V', 'J']
   proposition = ['B', 'V', 'R', 'J']
   
   indices = mastermind(combinaison_secrete, proposition)
   print(indices)  # Affiche ['O', 'O', 'X', 'X']
   
```



Astuce : pour tester des listes, on peut utiliser l'opérateur de comparaison "=="

## Annexe : Liste des couleurs disponibles

- Rouge (R)
- Bleu (B)
- Vert (V)
- Jaune (J)
- Orange (O)
- Rose (P)

------

Auteur : Florian Mathieu

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.