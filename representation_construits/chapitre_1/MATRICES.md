## Matrices en Python

> Une matrice est une collection de nombres disposés en lignes et en colonnes. On utilise les matrices pour représenter et manipuler des données qui ont plusieurs dimensions. 
>
> Et oui c'est pour ça que le film porte ce nom ! 
>
> Les matrices sont également très utilisées en mathématiques et en physique pour résoudre des systèmes d'équations linéaires, pour la transformation géométrique, en statistique, etc.

En python, une matrice est un *tableau de listes*

```python
matrice = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]]
```

On peut représenter l'instruction ci dessus de cette manière :

| i\j  | 0    | 1    | 2    | 3    |
| ---- | ---- | ---- | ---- | ---- |
| 0    | 1    | 2    | 3    | 4    |
| 1    | 5    | 6    | 7    | 8    |
| 2    | 9    | 10   | 11   | 12   |
| 3    | 13   | 14   | 15   | 16   |



Pour accéder à un élèment d'une matrice, s'agissant d'un tableau de listes, on utilisera deux *indices* :

- L'indice de la liste contenant l'élèment
- L'indice de l'élèment dans la liste selectionnée précédemment

On accède donc à l'élément situé en ligne **i** et en colonne **j** par 

```python
matrice[i][j]
```

Par exemple si l'on reprend le tableau ci dessus :

```python
matrice[2][3]
12
```

Cela retourne **12** car il s'agit de l'élément à la ***3ème ligne (indice 2)*** et à la ***4ème colonne (indice 3)*** de la matrice.

--------

### Question



Soit la matrice m = [["a","b"],["c","d"],["e","f"],["g","h"]]

Comment accéder à l'élément "e" ?

------

Auteur : Florian Mathieu

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.
