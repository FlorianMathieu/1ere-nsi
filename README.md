<<<<<<< HEAD
# Cours NSI

## Découpage

<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;margin:auto;" >
<body>
<tr>
<td style="text-align: center;"><a title="Représentation des données : Types et valeurs de base" href="representation_base"><img src='./assets/binary.svg' width="128px"/><br/>Types et valeurs de base</a></td>
<td style="border: none;text-align: center;"><a title="Représentation des données : Types construits" href="representation_construits"><img src='./assets/complex.svg' width="128px"/><br/>Types construits</a></td>
<td style="border: none;text-align: center;"><a title="Réseau et Web" href="reseau"><img src='./assets/network.svg' width="128px"/><br/>Réseau et Web</a></td>
<td style="border: none;text-align: center;"><a title="Architectures matérielles et systèmes d'exploitation" href="architecture"><img src='./assets/system.svg' width="128px"/><br/>Architectures matérielles et systèmes d'exploitation</a></td>
<td style="border: none;text-align: center;"><a title="Langages et programmation" href="programmation"><img src='./assets/python.svg' width="128px"/><br/>Langages et programmation</a></td>
<td style="border: none;text-align: center;"><a title="Algorithmique" href="algorithmes"><img src='./assets/algorithm.svg' width="128px"/><br/>Algorithmique</a></td>
</tr>
</body>
</table>


## Progression

| Séquence | Contenu                                                      | Projets associés           |
| -------- | ------------------------------------------------------------ | -------------------------- |
| 1 - a    | Introduction NSI, Découverte du markdown, jupyter notebook   | -                          |
| 1 - b    | Introduction à la programmation et révisions python - variables, instructions conditionnelles, boucles, fonctions | -                          |
| 2 - a    | Numération - Représentation des données : types et valeurs de base | Calculatrice               |
| 2 - b    | Représentation des données, types construits : listes, tuples, dictionnaires, données en table, structures imbriquées et compréhensions | Projet Pokedex, Zoo        |
| 3 - a    | Interactions entre l’homme et la machine sur le Web : html, css, javascript | Projet site web, projet JS |
| 3 - b    | Constructions élémentaires, spécifications, mise au point de programmes, utilisation de bibliothèques | Mise au point de fonctions |
| 4 - a    | Architecture Informatique : historique de l'informatique, modèle Von Neumann, os... | Linux, M999                |
| 4 - b    | Algorithmes : parcours séquentiel d'un tableau, algorithmes de tri, dichotomie... | Problème du crêpier        |
| 5 - a    | Algorithmes avancés : algorithmes gloutons, knn...           | Pokedex avancé, Choipeaux  |
| 5 - b    | Réseau : tcp / ip, dns...                                    | Projet Filius              |
| 6        | Projets finaux                                               | Au choix                   |



## Sitographie

- [Gitlab de David Landry](https://gitlab.com/david_landry/nsi)
- [Framagit de Christophe Mieszczak](https://framagit.org/tofmzk/informatique_git/-/tree/master/premiere_nsi)
- [Site de David Roche](https://pixees.fr/informatiquelycee/n_site/nsi_prem.html)
=======
# 1ere_NSI

Cours de première NSI de Florian Mathieu
>>>>>>> gitea/main
