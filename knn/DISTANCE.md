> Dans un repère à deux dimensions, si les coordonnées de deux points sont connues alors il est possible de calculer la longueur du segment qu'ils définissent, qu'on appellera ***distance*** 

D'ordre général, on parle de distance ***euclidienne*** que l'on peut noter comme ceci :

Distance AB de deux points A (x<sub>a</sub>, y<sub>a</sub>) et B (x<sub>b</sub>,y<sub>b</sub>) = 
$$
AB = \sqrt{(xa - xb)^2 + (ya - yb)^2}
$$


il s'agit de la mesure de distance la plus couramment utilisée, et elle est limitée aux vecteurs à valeurs réelles. 

Il existe également d'autres types de distance :

- Distance de Manhattan

il s'agit également d'une autre mesure de distance populaire, qui mesure la valeur absolue entre deux points. 

​	
$$
Distance (AB) = |xa - xb| + |ya - yb|
$$




- Distance de Hamming

Ici, il s'agit de compter le nombre de bits qui diffèrent entre deux mots binaires.
Exemple : si a = 0001 et b = 1101, alors d(a,b) = 2.

------------

Auteur : Florian Mathieu

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.
