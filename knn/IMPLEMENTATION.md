### Implémentation de l'algorithme des K plus proches voisins.

On pourrait essayer de penser à un algorithme simple pour pouvoir implémenter l'algorithme knn en python:

- Il nous faut une liste d'échantillons de taille *n*
- La donnée que l'on veut classer, appellons là *mystere*
- Un entier *k* plus petit que *n*
- La règle de calcul des distances

Il nous faudra donc répéter ces trois étapes suivantes pour faire fonctionner l'algorithme:

1. Trier les échantillons selon une distance croissante avec *mystere*
2. Un tableau contenant les *k* premiers voisins de la liste triée
3. Renvoyer ce tableau.

---------------

### Et en langage Python ?

Nous allons utiliser la bibliothèque `Matplotlib`

#### Pourquoi utiliser`matplotlib` ?

- **Visualiser les données** : Avant de plonger dans des algorithmes complexes, il est essentiel de comprendre les données avec lesquelles on travaille. Un bon graphique peut révéler des tendances, des anomalies, des clusters ou des relations entre les variables bien mieux qu'une simple inspection des chiffres.
- **Communiquer des résultats** : Les graphiques aident à communiquer les résultats d'une analyse de manière efficace et intuitive, que ce soit entre scientifiques ou pour présenter à un public non spécialiste.

### Premiers Pas avec `matplotlib`

Pour commencer, il faut installer `matplotlib` si ce n'est pas déjà fait. Cela se fait généralement via pip :

```python
pip install --upgrade --proxy=172.16.0.253:3128 matplotlib
```

Ensuite, pour utiliser `matplotlib`, on commence par importer le module `pyplot`, souvent sous l'alias `plt` :

```python
import matplotlib.pyplot as plt
```

### Exemple de Base

Imaginons que l'on souhaite représenter les statistiques d'attaque et de défense de quelques Pokémon. 

```python
import matplotlib.pyplot as plt

# Supposons que ces listes contiennent les statistiques d'attaque et de défense de quelques Pokémon
attaques = [55, 75, 150, 45]
defenses = [45, 60, 50, 65]
noms = ['Pikachu', 'Bulbasaur', 'Charizard', 'Squirtle']

plt.figure(figsize=(10, 5)) # Définit la taille de la figure
plt.scatter(attaques, defenses, color='red') # Crée un nuage de points avec les statistiques d'attaque et de défense

# Ajoute des titres et des étiquettes
plt.title('Attaque vs Défense des Pokémon')
plt.xlabel('Attaque')
plt.ylabel('Défense')

# Ajoute des annotations pour chaque point
for i, nom in enumerate(noms):
    plt.annotate(nom, (attaques[i], defenses[i]))

plt.grid(True) # Ajoute une grille pour une meilleure lisibilité
plt.show() # Affiche le graphique
```

Ce code génère un graphique en nuage de points où chaque point représente un Pokémon, avec son niveau d'attaque sur l'axe des x et son niveau de défense sur l'axe des y. Les annotations permettent d'identifier chaque Pokémon sur le graphique.

--------

Auteur : Florian Mathieu

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.
