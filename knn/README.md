> Dans le domaine de l'apprentissage automatique dit "Machine Learning", l'algorithme des k plus proches voisins est l'un des plus utilisés.

### Le programme

![bo](assets/bo.png)

------

### Un peu d'histoire...

Même si les [Large Language Models (LLM)](https://datascientest.com/large-language-models-tout-savoir) ont démocratisé ce genre d'outil, l'apprentissage automatique ne date pas d’hier, puisque le terme de *machine learning* a été utilisé pour la première fois par l’informaticien américain *Arthur Samuel* en 1959. Puis, début 2000, l'arrivée des *Big Datas* va contribuer à l'explosion de ce genre de modèle.

-------------

### Classification

- Permet de prédire si un élèment est membre d'un groupe ou d'une catégorie donnée
- Une classe permet une identification de groupes avec des profils particuliers
- Offre une possibilité de décider de l'appartenance d'une entitié à une classe
- Dans notre exemple, on parlera de classification ***supervisée*** c'est à dire que l'on connait déjà les différentes classes à l'avance

Parmi les modèles et méthodes de classification, on peut citer :

- K-NN Voisins
- Arbres de décision
- Réseaux de neuronnes

----------

### Exemple de Problème de classification avec K-NN

L'algorithme des *k plus proches voisins* permet de résoudre certains problèmes, notamment ceux qui proposent de classer des données et élèments.

Il s'agit d'une méthode de raisonnement à partir de cas connus : ce modèle va aider à prendre des décisions en recherchant un ou des cas similaires déjà résolus.

Attention, ici, il n'y a pas d’étape d ’apprentissage: uniquement une construction d ’un ***modèle*** à partir d’un échantillon d'apprentissage.

### Qu'est ce qu'un modèle ? 

Il s'agit d'une représentation simplifiée de la réalité en vue de réaliser un traitement avec un ordinateur.

***Dans ce cas, notre modèle = échantillon d’apprentissage + fonction de distance + fonction de choix de la classe en fonction des classes des voisins les plus proches.***



### Pour quels types de variables ?

- Qualitatives 
- Quantitatives

------------

### Exemple de l'implémentation de l'algorithme des k plus proches voisins

Le Professeur Chen, inventeur du Pokédex, utilise cet algorithme afin que son appareil puisse prédire quel pokémon se trouve devant lui.

<img src="assets/chen.png" alt="chen" style="zoom:67%;" />

Pour simplifier, imaginons que les Pokemons ne possèdent que deux caractéristiques : leurs points de vie et leur valeur d'attaque. On peut prendre deux types pour commencer.

| Nom           | Écayon | Deoxys | Éoko | Groret | Taraud |
| ------------- | ------ | ------ | ---- | ------ | ------ |
| Points de vie | 49     | 50     | 80   | 90     | 90     |
| Attaque       | 49     | 95     | 45   | 75     | 75     |
| Type          | Eau    | Psy    | Psy  | Psy    | Eau    |

------------

- Nous pouvons utiliser cet échantillon afin de prédire la classification d'un Pokémon mystère, selon ses points de vie et sa valeur d'attaque, en représentant ces données sous la forme d'un graphique.

![echantillon](assets/echantillon_pokemon.png)

------------

### Prédiction

À partir des données du diagramme, on veut prédire la classe d'un pokémon ayant 65 points de vie et 40 en attaque.

![echantillon_2](assets/echantillon_pokemon_2.png)

Il devrait donc se trouver dans zone. On peut alors trouver ses cinq ou six plus proches voisins.

- Parmi ces voisins se trouvent deux Pokémons de type Eau, et trois de types Psy.
- Le Pokémon mystère sera donc probablement de type Psy !

-------

### Formulation de l'algorithme

Pour rendre cette classification automatique, nous allons utiliser un algorithme python.

D'après vous, que faut-il pour pouvoir prédire la classe d'un Pokemon donné ?

- Échantillon de Pokemon
- Un pokemon *mystère* dont on souhaite prédire la classification
- la valeur de *k*

Une fois ces données modélisées, on peut formuler notre algorithme:

- Trouver dans l'échantillon, les *k* plus proches voisins du pokemon *mystère*
- Trouver la classification majoritaire parmi les voisins les plus proches
- Renvoyer cette classification

### Pour résumer...

L'algorithme des k plus proches voisins a pour objectif d'identifier les voisins les plus proches d'un point de requête donné, afin d'attribuer une classe à ce point

--------------

Auteur : Florian Mathieu

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.

