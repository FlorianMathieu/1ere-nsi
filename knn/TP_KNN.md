## Choixpeau Magique et KNN

## 



<img src="assets/choixpeau.jpeg" alt="ChoixpeauMagique" style="zoom: 25%;" />

Choixpeau Magique 

*Par Suzelfe — Travail personnel, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=41552140*

### Contexte

À l'entrée de l'école de Poudlard, le Choixpeau magique répartit les élèves dans les différentes maisons (Gryffondor, Serpentard, Serdaigle et Poufsouffle) en fonction de leur courage, leur loyauté, leur sagesse, et leur malice.

### Données disponibles

Le Choixpeau magique dispose d'un [fichier csv](choixpeauMagique.csv)  avec les données d'un échantillon d'élèves. Les 6 premières lignes du fichier sont les suivantes :

| Nom      | Courage | Loyauté | Sagesse | Malice | Maison     |
| -------- | ------- | ------- | ------- | ------ | ---------- |
| Adrian   | 9       | 4       | 7       | 10     | Serpentard |
| Andrew   | 9       | 3       | 4       | 7      | Gryffondor |
| Angelina | 10      | 6       | 5       | 9      | Gryffondor |
| Anthony  | 2       | 8       | 8       | 3      | Serdaigle  |
| Arthur   | 10      | 4       | 2       | 5      | Gryffondor |



Et voici les élèves que le Choipeaux magique doit orienter :

| Nom      | Courage | Loyauté | Sagesse | Malice |
| -------- | ------- | ------- | ------- | ------ |
| Hermione | 8       | 6       | 6       | 6      |
| Drago    | 6       | 6       | 5       | 8      |
| Cho      | 7       | 6       | 9       | 6      |
| Cédric   | 7       | 10      | 5       | 6      |



### Partie 1 : Modéliser un élève

Chaque élève est modélisé par un dictionnaire contenant les valeurs de courage, de loyauté, de sagesse, et de malice.

### Exemple

```python
adrian = {"nom": "Adrian", "courage": 9, "loyauté": 4, "sagesse": 7, "malice": 10, "maison": "Serpentard"}
hermione = {"nom": "Hermione", "courage": 8, "loyauté": 6, "sagesse": 6, "malice": 6}
```

1. Donner la modélisation de l'élève Anthony
2. On décide d'utiliser la distance de Manhattan pour calculer la distance entre deux élèves telle que :

$$
distance(élève1,élève2) = |c1 - c2| + |l1-l2| + |s1 - s2| + |m1 - m2|
$$

3. Avec cette formule, vérifier que la distance entre Hermione et Adrian est égale à 8
4. Quelle est la distance entre Arthur et Drago ?
5. Écrire le code d'une fonction distance qui prend deux élèves en paramètre et qui renvoie la distance entre ces deux élèves.

----------

### Partie 2 : Charger les données en table





```python
import csv
def charger_table(nom_fichier):
    """
: fonction charger_table : Permet de charger une liste d'élèves à partir d'un fichier CSV
: param nom_fichier : le nom d'un fichier CSV
: return table : la liste des élèves
"""

    table= []
    with open(nom_fichier, 'r',newline="",encoding="utf-8") as csvfile:
        eleve_reader = csv. reader(csvfile, delimiter=";")
        eleve_reader.__next__()
        for eleve in eleve_reader:
            table.append({ "nom": eleve[0],
                           "courage": int(eleve[1]),
                           "loyauté": int(eleve[2]),
                           "sagesse":int(eleve[3]),
                           "malicez":int(eleve[4]),
                           "maison" : eleve[5]})
    return table

```

L'instruction suivante permet de charger les informations dans une variable **poudlard** à partir d'un fichier choixpeauMagique. csv qui se trouve dans le répertoire courant (n'oubliez pas de l'y déposer ! ) :

```python
poudlard = charger_table("choixpeauMagique.csv")
```

1. Dans le code de la fonction charger _table, à la ligne 14, quel est le type de la variable eleve[0] ?

2. Quel est le type de int ( eleve [ 0] ) ? Pourquoi a-t-on besoin de la fonction int() ici?
3. Quel est le type de la variable poudlard ?



---------

### Partie 3 Trouver la maison majoritaire



On souhaite écrire le code d'une fonction qui prend en paramètre une liste d'élèves et qui renvoie la maison la plus représentée dans cette liste. Pour cela, on propose d'utiliser une fonction auxilliaire dont le code est le suivant :



```python
def frequence_des_maisons(table):
    """
Paramètre: une liste d'élèves, chaque élève étant modélisé par un dictionnaire
Résultat: un dictionnaire dont les clés sont les maisons et les valeurs, le nombre de fois où cette maison apparaît.
"""
    frequences={}
    for eleve in table:
        maison = eleve["maison"]
        if maison in frequences.keys():
            frequences[maison] = frequences[maison] + 1
        else:
          frequences[maison] = 1
    return frequences
```

1. Écrire le code de la fonction *maison_majoritaire* qui prend une liste d'élèves en paramètre et qui renvoie le nom de la maison la plus représentée. 



-------



### Partie 4 Sept plus proches voisins



1. Écrire et compléter en pseudo code l'algorithme qui suit : 

   Données:

   - table: une liste d'élèves;
   -  nouveau: un nouvel élève qui n'a pas encore de maison.
   - Résultat : les 7 plus proches voisins du nouveau.

   Algorithme :

   - à compléter
   - à compléter
   - à compléter

2. Implémenter cet algorithme en Python

-------



### Partie 5 Attribuer une maison



Le Choixpeau magique décide d'utiliser un algorithme de prédiction pour choisir la maison qui sera attribuée aux nouveaux élèves. Voici cet algorithme :

Données:

	- table est une liste d'élèves;
	- un nouvel élève qui n'a pas encore de maison.
	- Résultat : la maison du nouvel élève.

Algorithme :

- Trouver dans la table les 7 plus proches voisins du nouvel élève.
- Parmi ces proches_voisins, trouver la maison majoritaire.
- Renvoyer la maison_majoritaire.
- Implémenter cet algorithme en Python 



----------

Sources : 

- Prépabac NSI (Édition Hatier)
- [Site de l'académie de Nantes](https://www.pedagogie.ac-nantes.fr/enseignements-informatiques/enseignement/nsi/mini-projet-choixpeau-magique-1447870.kjsp?RH=1158678510343)
- [Gitlab de David Landry](https://gitlab.com/david_landry/nsi)