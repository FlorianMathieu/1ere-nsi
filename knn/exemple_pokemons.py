import matplotlib.pyplot as plt

# On initialise des tableaux contenants des statistiques de Pokemon
attaques = [55, 75, 150, 45]
defenses = [45, 60, 50, 65]
noms = ['Pikachu', 'Bulbizarre', 'Salameche', 'Carapuce']

plt.figure(figsize=(10, 5)) # Définit la taille de la figure
plt.scatter(attaques, defenses, color='red') # Crée un nuage de points avec les statistiques d'attaque et de défense

# Ajoute des titres et des étiquettes
plt.title('Attaque vs Défense des Pokémon')
plt.xlabel('Attaque')
plt.ylabel('Défense')

# Ajoute des annotations pour chaque point
for i, nom in enumerate(noms):
    plt.annotate(nom, (attaques[i], defenses[i]))

plt.grid(True) # Ajoute une grille pour une meilleure lisibilité
plt.show() # Affiche le graphique

