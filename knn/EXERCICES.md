### Exercices

1. **Écrire une fonction `calculer_distance`** qui prend en entrée les caractéristiques de deux Pokémon et retourne la distance euclidienne entre eux. 
2. **Tester la fonction** avec des exemples simples pour vérifier son bon fonctionnement

Conseil : n'oubliez pas d'importer la bibliothèque `pandas`qui permet de travailler avec des fichiers csv (le cours se trouve [ici](../données_en_table))

Le fichier [pokemons.csv](pokemons.csv) qui va avec.

Ne pas oublier d'installer la bibliothèque pandas

```python
pip install --upgrade --proxy=172.16.0.253:3128 pandas
```



```python
import pandas as pd

pokemons = pd.read_csv('chemin/vers/pokemons.csv')
```

```python
import numpy as np

def calculer_distance(pokemon1, pokemon2):
    # Utilise numpy pour calculer la distance euclidienne
    distance = np.sqrt(np.sum(np.square(np.array(pokemon1) - np.array(pokemon2))))
    return distance

# Exemple d'utilisation
pokemon1 = [60, 62]  # Exemple de stats pour le Pokémon 1
pokemon2 = [85, 80]  # Exemple de stats pour le Pokémon 2

distance = calculer_distance(pokemon1, pokemon2)
print(f"La distance entre les deux Pokémon est : {distance}")

```

