import pandas as pd

# Chargement des données
pokemons = pd.read_csv('pokemons.csv')

# Afficher les premières lignes pour vérification
print(pokemons.head())


import matplotlib.pyplot as plt

# Filtrons les données pour n'avoir que les Pokémon de type Eau
eau_pokemons = pokemons.dropna(subset=['Pokemons de type Eau'])

# Création du graphe
plt.scatter(eau_pokemons['points de vie'], eau_pokemons['Pokemons de type Eau'])
plt.title('Points de vie vs Attribut Eau des Pokémon')
plt.xlabel('Points de vie')
plt.ylabel('Attribut Eau')
plt.show()


import numpy as np

# Ajoutons un Pokémon "mystère"
pokemon_mystere = [70, 65]

def calculer_distance(pokemon1, pokemon2):
    return np.sqrt(np.sum(np.square(np.array(pokemon1) - np.array(pokemon2))))

# Calculons la distance de chaque Pokémon de type Eau au Pokémon "mystère"
distances = eau_pokemons.apply(lambda row: calculer_distance([row['points de vie'], row['Pokemons de type Eau']], pokemon_mystere), axis=1)

# Ajoutons ces distances au DataFrame
eau_pokemons['Distance au mystère'] = distances

# Affichons les Pokémon triés par leur distance au mystère
print(eau_pokemons.sort_values(by='Distance au mystère').head())
